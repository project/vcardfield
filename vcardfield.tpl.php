<?php
  /*
    field array
    $$variables


    vCard available values
      $variables['prefix']
      $variables['first_name']
      $variables['last_name']
      $variables['suffix']
      $variables['full_name']
      $variables['title']
      $variables['organization']
      $variables['address_type']
      $variables['address']
      $variables['city']
      $variables['region']
      $variables['country']
      $variables['phone_default']
      $variables['phone_cell']
      $variables['phone_fax']
      $variables['phone_home']
      $variables['email']
      $variables['link'] // contact URL


    Vcard link info
      $variables['label'] - Link label user data
      $variables['vcard_url']  - URL for custom formatting
      $variables['vcard_link']  - full link to get vcard
  */
?>
<div class="vcardfield-wrapper <?php echo $classes.' '.$variables['zebra']; ?>">

    <?php echo $variables['full_name']; ?><br />
    <?php echo $variables['vcard_link']; ?>

</div>
